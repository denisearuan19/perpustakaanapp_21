from app.models.borrows_model import database
from app.models.customers_model import database as cust_db
from flask import jsonify, request
from flask_jwt_extended import *
import json
import datetime
import requests

mysqldb = database()
cust_db = cust_db()


@jwt_required()
def shows():
    # ambil payload kita dari token
    params = get_jwt_identity()
    dbresult = mysqldb.showBorrowByEmail(**params)
    result = []
    if dbresult is not None:
        for item in dbresult:
            id = json.dumps({"id": items[4]})
            bookdetail = getBookById(id)
            user = {
                "usernmae": items[0],
                "borrowid": items[1],
                "borrowdate": items[2],
                "borrowid": items[4],
                "bookname": items[5],
                "author": bookdetail["pengarang"],
                "releaseyear": bookdetail["tahunterbit"],
                "genre": bookdetail["genre"]
            }


@jwt_required()
def insert():
    token = get_jwt_identity()
    userid = cust_db.showUserByEmail(**token)[0]
    borrowdate = datetime.datetime.now().isoformat()
    id = json.dumps({"id": params["bookid"]})
    bookname = getBookById(id)["name"]
    params.update(
        {
            "userid": userid,
            "borrowdate": borrowdate,
            "bookname": bookname,
            "isactive": 1
        }
    )
    mysqldb.insertBorrow(**params)
    mysqldb.dataCommit()
    return jsonify({"message": "Success"})


@jwt_required()
def changeStatus():
    mysqldb.updateBorrow(**params)
    mysqldb.dataCommit()
    return jsonify({"message": "Success"})


def getBookById(data):
    book_data = request.get(url="http:/localhost:800/bookbyid", data=data)
    return book_data.json()
