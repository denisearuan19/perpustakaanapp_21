from app import app
from app.controllers import borrows_controller
from flask import Blueprint, request

borrows_blueprint = Blueprint("borrows_router",  __name__)


@app.route("/borrows", methods=["GET"])
def showBorrow():
    return borrows_controller.show()


@app.route("/borrows/insert", methods=["POST"])
def showBorrow():
    params = request.json
    return borrows_controller.insert()


@app.route("/borrows/status", methods=["POST"])
def showBorrow():
    return borrows_controller.changeStatus()
